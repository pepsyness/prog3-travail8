﻿<?php
include_once('Classes/Usager.class.php');
include('Includes/fonctions.php');
session_start();

$login_name = "";
$login_pass = "";

if (isset($_SESSION["usager_courant"])) {
    // Déjà identifié
}
else {
    if (isset($_POST["input_login"]) && $_POST["input_login"] != null && isset($_POST["input_pass"]) && $_POST["input_pass"] != null) {
        // Connect DB et vérif
        $login_name = $_POST["input_login"];
        $login_pass = $_POST["input_pass"];
        $db = mysql_connect("info-web02.cegep-chicoutimi.qc.ca", "1432744", "BOIS22128109");
        mysql_select_db("p3-1432744", $db);
        $requete = "SELECT * FROM tp8_usagers WHERE NomUtilisateur='$login_name' AND MotDePasse='$login_pass'";
        $result = mysql_query($requete) or die("Erreur SQL !<br />".$requete."<br />".mysql_error());
        $count = mysql_num_rows($result); // Login ok = 1 (une colonne renvoyée ou plus si duplicata dans la db)
        if ($count > 0) {
            // Login ok. Récupérer le reste des données.
            while ($row = mysql_fetch_assoc($result)) {
                $usager_courant = new Usager($row["ID"], $row["NomUtilisateur"], $row["MotDePasse"], $row["Nom"], $row["Prenom"], $row["Courriel"], true, $row["IsAdmin"]);
                $_SESSION["usager_courant"] = $usager_courant;
            }
            unset($_SESSION["nbr_essais"]);
            //echo "Bienvenue " . $_SESSION["login_name"] . "!";
        }
		else {
            // Login + Pass mismatch: redirect sur login.php
            essais_erreurs();
            Header("Location: login.php");
        }
        mysql_close();
    }
	else {
        // Redirect au début
        essais_erreurs();
        Header("Location: login.php");
    }
}

//Cookie
include('Includes/cookies.php');

?>

<!DOCTYPE html> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">

<head>
    <title>Travail 8</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <link type="text/css" rel="stylesheet" href="css/style.css" />
</head>

<body bgcolor="<?php echo $couleur_fond; ?>">
    <div class="div_principal">
        <?php include( 'Includes/header.php') ?>
        <?php include( 'Includes/menus.php') ?>
        <div class="div_contenu_cadre">
            <span class="span_titre">Vos informations d'usager</span>
            <br />
            <br />
            <table>
                <tr>
                    <td class="td_nom_attribut">ID:</td>
                    <td><?php echo $_SESSION[ "usager_courant"]->getID(); ?></td>
                </tr>
                <tr>
                    <td class="td_nom_attribut">Prénom:</td>
                    <td><?php echo $_SESSION[ "usager_courant"]->getPrenom(); ?></td>
                </tr>
                <tr>
                    <td class="td_nom_attribut">Nom:</td>
                    <td><?php echo $_SESSION[ "usager_courant"]->getNom(); ?></td>
                </tr>
                <tr>
                    <td class="td_nom_attribut">Courriel:</td>
                    <td><?php echo $_SESSION[ "usager_courant"]->getCourriel(); ?></td>
                </tr>
                <tr>
                    <td class="td_nom_attribut">Nom d'utilisateur:</td>
                    <td><?php echo $_SESSION[ "usager_courant"]->getLogin_name(); ?></td>
                </tr>
                <tr>
                    <td class="td_nom_attribut">Mot de passe:</td>
                    <td><?php echo $_SESSION[ "usager_courant"]->getLogin_pass(); ?></td>
                </tr>
            </table>
        </div>
        <?php include( 'Includes/footer.php') ?>
    </div>
</body>

</html>