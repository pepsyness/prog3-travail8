﻿<?php
include_once('Classes/Usager.class.php');
session_start();
include('Includes/fonctions.php');

if (isset($_SESSION["usager_courant"])) {
	// Déjà identifié
}

else {
	// Redirect au début
	essais_erreurs();
	Header("Location: login.php");
}

// Cookie
include('Includes/cookies.php');

?>

<!DOCTYPE html> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
	<head>
		<title>Travail 8</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<link type="text/css" rel="stylesheet" href="css/style.css" />
	</head>
	<body bgcolor="<?php echo $couleur_fond; ?>">
		<div class="div_principal">
			<?php include('Includes/header.php') ?>
			<?php include('Includes/menus.php') ?>
			<div class="div_contenu_cadre">
				<span class="span_titre">Configuration</span>
				<br />
				<br />
				<form method="POST" action="config.php">
				<table class="table_config">
					<tr>
						<td><b><font color="black">Votre couleur de fond</font></b></td>
						<td></td>
					</tr>
					<tr>
						<td bgcolor="blue">Bleu</td>
						<td bgcolor="blue"><input type="radio" name="couleur_fond" value="blue"></td>
					</tr>
					<tr>
						<td bgcolor="red">Rouge</td>
						<td bgcolor="red"><input type="radio" name="couleur_fond" value="red"></td>
					</tr>
					<tr>
						<td bgcolor="green">Vert</td>
						<td bgcolor="green"><input type="radio" name="couleur_fond" value="green"></td>
					</tr>
					<tr>
						<td bgcolor="yellow">Jaune</td>
						<td bgcolor="yellow"><input type="radio" name="couleur_fond" value="yellow"></td>
					</tr>
					<tr>
						<td bgcolor="black"><font color="white">Noir</font></td>
						<td bgcolor="black"><input type="radio" name="couleur_fond" value="black"></td>
					</tr>
					<tr>
						<td bgcolor="white">Blanc</td>
						<td bgcolor="white"><input type="radio" name="couleur_fond" value="white"></td>
					</tr>
					<tr>
						<td rowspan="2"><input type="submit" value="Enregistrer"></td>
						<td></td>
					</tr>
				</table>
				</form>
			</div>
			<?php include('Includes/footer.php') ?>
		</div>
	</body>
</html>
