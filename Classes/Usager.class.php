<?php
class Usager {
	private $id;
	private $actif;
	private $login_name;
	private $login_pass;
	private $nom;
	private $prenom;
	private $courriel;
	private $isAdmin;
	
	public function __construct($id, $login_name, $login_pass, $nom, $prenom, $courriel, $actif, $isAdmin) {
		$this->actif = $actif;
		$this->login_name = $login_name;
		$this->login_pass = $login_pass;
		$this->nom = $nom;
		$this->prenom = $prenom;
		$this->courriel = $courriel;
		$this->id = $id;
		$this->isAdmin = $isAdmin;
	}
	
	//GETS
	public function getActif() {
		return $this->actif;
	}
	
	public function getLogin_name() {
		return $this->login_name;
	}
	
	public function getLogin_pass() {
		return $this->login_pass;
	}
	
	public function getNom() {
		return $this->nom;
	}
	
	public function getPrenom() {
		return $this->prenom;
	}
	
	public function getCourriel() {
		return $this->courriel;
	}
	
	public function getID() {
		return $this->id;
	}
	
	public function getIsAdmin() {
		return $this->isAdmin;
	}
	
	//SETS
	public function setLogin_name($newLogin_name) {
		$this->login_name = $newLogin_name;
	}
	
	public function setLogin_pass($newLogin_pass) {
		$this->login_pass = $newLogin_pass;
	}
	
	public function setNom($newNom) {
		$this->nom = $newNom;
	}
	
	public function setPrenom($newPrenom) {
		$this->prenom = $newNom;
	}
	
	public function setCourriel($newCourriel) {
		$this->courriel = $newCourriel;
	}
	
	public function setID($newID) {
		$this->id = $newID;
	}
	
	public function setIsAdmin($newState) {
		$this->isAdmin = $newState;
	}
	
	public function desactiver() {
		$this->actif = false;
	}
}
?>