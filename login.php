﻿<?php 
session_start();
if (isset($_SESSION[ "usager_courant"])) {
	unset($_SESSION[ "usager_courant"]);
}
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">

<head>
    <title>Travail 8</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <link type="text/css" rel="stylesheet" href="css/style.css" />
</head>

<body>
    <br />
    <form method="POST" action="home.php">
        <table class="table_login">
            <tr>
                <td colspan="2"><h2>Identification</h2></td>
            </tr>
            <tr>
                <td>Votre nom d'usager:</td>
                <td><input type="text" name="input_login" /></td>
            </tr>
            <tr>
                <td>Votre mot de passe:</td>
                <td><input type="password" name="input_pass" /></td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" value="Connexion" /></td>
        </table>
    </form>
    <br />
    <div class="div_tentatives">
        <span class="span_tentatives"><?php if (isset($_SESSION[ "nbr_essais"])) { echo "Tentative #" . $_SESSION[ "nbr_essais"]; } ?></span>
    </div>
</body>
</html>